import {hashHistory} from 'react-router';
import initialState from './initialState';
import $ from 'jquery';
import toastr from 'toastr';


export default function applicationReducers(state = initialState, action) {

    switch (action.type) {

        case 'ON_LEFT_MENU_SHOW_OR_HIDE': {

            const newState = JSON.parse(JSON.stringify(state));
            newState.leftMenuStatus = !newState.leftMenuStatus;
            if (newState.leftMenuStatus === false) {
                newState.mainOccupancyStyle = 'col-xs-12 col-sm-12 col-md-12';
                newState.leftMenuStyle = 'hide';
            } else {
                newState.mainOccupancyStyle = 'col-xs-10 col-sm-10 col-md-10';
                newState.leftMenuStyle = 'col-xs-2 col-sm-2 col-md-2';
            }
            return newState;
        }
        case 'ON_RESET_STATE': {

            const newState = JSON.parse(JSON.stringify(state));
            console.log("initialState", initialState)

            return initialState;
        }
        case 'SHOW_LOGIN': {
            const newState = JSON.parse(JSON.stringify(state));
            newState.open = true;

            return newState;
        }
        case 'HIDE_LOGIN': {
            const newState = JSON.parse(JSON.stringify(state));
            newState.open = false;

            return newState;
        }
        default:
            return state;
    }
}