import React,{PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actionMaster from '../../actions/uiAction';


export default class Footer extends React.Component {
    constructor(){
        super();
    }


    render(){
        return(
            <div>
                <footer style={{height:'100px'}}>
                    <div className="container">
                        <div className="row">
                            <div className="col-md-3">
                                <span className="footerHeader">Visit Our ShowRoom</span>
                                <p>
                                    <span className="addressH">Cras vehicula nunc</span>
                                    <span className="addressH">Nulla ut risus</span>
                                    <span className="addressH">Etiam in cursus 60 017 89</span>
                                </p>
                            </div>
                            <div className="col-md-3">
                                <span className="footerHeader">Get In Touch</span>
                                <p>
                                    <span className="addressH">40 84 201 27 53</span>
                                    <span className="addressH"><a href="mailto:info@test.com">info@whiteandblack.com</a></span>
                                </p>
                            </div>
                            <div className="col-md-6 text-right">
                                <span className="copyRightH">@ 2018 Black & White</span>
                            </div>
                        </div>
                    </div>
                </footer>

            </div>
        );
    }

}

