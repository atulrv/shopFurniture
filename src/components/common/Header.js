import React,{PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actionMaster from '../../actions/uiAction';
import {Link} from 'react-router';

class Header extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    componentDidMount() {

    }

    render() {
        return (
            <div className="navbar-inverse navbar-fixed-top " style={{backgroundColor:'white'}} >
                <div className="cartH " style={{marginRight:'35px'}}>
                    <a href="#" className="cart">
                        <span className="bubbleH">2</span>
                        <i className="glyphicon glyphicon-shopping-cart" aria-hidden="true"></i>
                    </a>
                </div>
                <header>

                    <div className="container-fluid">
                        <div className="row">
                            <div className="className-xs-12">
                                <div className="text-center">
                                    <a href="index.html" className="logo">Black & White</a>
                                </div>
                            </div>
                        </div>

                        <div className="row">
                            <nav className="subPageNav">

                                <ul>
                                    <li><Link  to="/">Home</Link></li>

                                    <li><a href="#">News</a></li>

                                    <li>
                                        <Link  to="Rugs">Rugs</Link>
                                    </li>


                                    {/*<li ><a href="#">Rugs</a></li>*/}
                                    <li><a href="#">Cushions</a></li>
                                    <li><a href="#">OverClocking & Plain</a></li>
                                    <li><a href="#">Benches</a></li>
                                    <li><a href="#">Furnishings</a></li>
                                    <li><a href="#">Inspiration</a></li>
                                    <li><a href="#" data-toggle="modal" data-toggle="modal" data-target="#myModal">Login & Signup</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </header>
            </div>


        )
            ;
    }

}

Header.propTypes = {
    actionMaster: PropTypes.object,
};

function mapStateToProps(state, ownProps) {
    return state.application;
}

function mapDispatchToProps(dispatch) {
    return {
        actionMaster: bindActionCreators(actionMaster, dispatch),
        //actionOrg: bindActionCreators(actionOrg, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
