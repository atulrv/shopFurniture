import React, {PropTypes} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from 'redux';
import * as actionMaster from '../../actions/uiAction';
import Header from '../common/Header';
import Footer from "../common/Footer";


class WelcomPage extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    componentDidMount() {

    }

    render() {



        return (
            <div className="mainH">
                <Header/>
                <div id="myCarousel" className="carousel slide" data-ride="carousel" style={{marginTop:'100px'}}>

                    <ol className="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" className="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>

                    <div className="carousel-inner">
                       <div className="item active"> <img src={require('../../images/slider_1.jpg')} alt="logo" className="brand-logo" title="Consectetur adipiscing elit"/></div>
                        <div className="item"><img src={require('../../images/slider_2.jpg')} alt="logo" className="brand-logo" title="Consectetur adipiscing elit"/></div>
                        <div className="item"><img src={require('../../images/slider_3.jpg')} alt="logo" className="brand-logo" title="Consectetur adipiscing elit"/></div>
                    </div>


                    <a className="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span className="glyphicon glyphicon-chevron-left"></span>
                        <span className="sr-only">Previous</span>
                    </a>
                    <a className="right carousel-control" href="#myCarousel" data-slide="next">
                        <span className="glyphicon glyphicon-chevron-right"></span>
                        <span className="sr-only">Next</span>
                    </a>

                </div>



                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="heading">
                                <h1>FEATURED PRODUCTS</h1>
                                <span className="subTxt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vehicula, metus vitae interdum tempus, metus mauris facilisis eros, a fermentum lectus sem vitae dui.</span>
                            </div>
                        </div> 
                    </div>

                    <div className="row">
                        <div className="col-sm-6 col-md-4">
                            <div className="featuredBox">
                                <h3>RUGS</h3>
                                <div className="roundImg">
                                    <a href="productPage.html">
                                        <img src={require('../../images/featureImg_1.jpg')} alt="" />
                                    </a>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </div> 
                                <a href="productPage.html" className="splBtn">Products</a>
                            </div> 
                        </div>
                        <div className="col-sm-6 col-md-4">
                            <div className="featuredBox">
                                <h3>CUSHIONS</h3>
                                <div className="roundImg">
                                    <a href="productPage.html">
                                        <img src={require('../../images/featureImg_2.jpg')} alt="" />
                                    </a>
                                    <p>consectetur adipiscing elit. Ut vehicula, metus vitae interdum tempus.</p>
                                </div> 
                                <a href="productPage.html" className="splBtn">Products</a>
                            </div>
                        </div>
                        <div className="col-sm-6 col-md-4">
                            <div className="featuredBox">
                                <h3>THROWS</h3>
                                <div className="roundImg">
                                    <a href="productPage.html">
                                        <img src={require('../../images/featureImg_3.jpg')} alt="" />

                                    </a>
                                    <p>Suspendisse vitae nunc in ipsum pulvinar luctus vel nec nunc.</p>
                                </div>
                                <a href="productPage.html" className="splBtn">Products</a>
                            </div> 
                        </div>
                    </div> 

                    <div className="row">
                        <div className="col-sm-12">
                            <div className="heading">
                                <h1>Lorem Ipsum Dolor</h1>
                            </div>
                        </div>
                    </div> 

                    <div className="row">
                        <div className="col-md-8">
                            <div className="contentH">
                                <h5>RUGS</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer volutpat vehicula erat eget aliquam. Sed vel metus ac lacus euismod dignissim a vitae turpis. Suspendisse non cursus dolor. Curabitur luctus quam a arcu scelerisque, ac interdum elit luctus. Suspendisse potenti. Duis vel diam at dolor facilisis sollicitudin in sed sem. Quisque et leo eros. Nam fermentum ligula ipsum, eu sollicitudin purus rhoncus consectetur. Etiam non dui faucibus, mattis odio a, pretium felis.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer volutpat vehicula erat eget aliquam. Sed vel metus ac lacus euismod dignissim a vitae turpis. Suspendisse non cursus dolor. Curabitur luctus quam a arcu scelerisque, ac interdum elit luctus. Suspendisse potenti. Duis vel diam at dolor facilisis sollicitudin in sed sem. Quisque et leo eros. Nam fermentum ligula ipsum, eu sollicitudin purus rhoncus consectetur. Etiam non dui faucibus, mattis odio a, pretium felis.</p>
                            </div>
                        </div> 

                        <div className="col-md-4">
                            <div className="contentH">
                                <h5>THROWS</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer volutpat vehicula erat eget aliquam. Sed vel metus ac lacus euismod dignissim a vitae turpis. Suspendisse non cursus dolor. Curabitur luctus quam a arcu scelerisque, ac interdum elit luctus. Suspendisse potenti. Duis vel diam at dolor facilisis sollicitudin in sed sem. Quisque et leo eros. Nam fermentum ligula ipsum, eu sollicitudin purus rhoncus consectetur. Etiam non dui faucibus, mattis odio a, pretium felis.</p>
                            </div>
                        </div>
                    </div> 

                    <div className="row">
                        <div className="col-sm-12">
                            <div className="heading">
                                <h1>Recently Viewed</h1>
                            </div>
                        </div>
                    </div> 

                    <div className="row">
                        <div className="col-md-12 mb20">
                            <ul className="recentViewSlider">
                                <li>
                                    <a href="#">
                                        <img src={require('../../images/carousel_Img_1.jpg')} alt="" />
                                        <span className="recentTxt">Lorem ipsum dolor erat eget aliquam.</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src={require('../../images/carousel_Img_2.jpg')} alt="" />
                                        <span className="recentTxt">Lorem ipsum dolor erat eget aliquam.</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src={require('../../images/carousel_Img_3.jpg')} alt="" />
                                        <span className="recentTxt">Lorem ipsum dolor erat eget aliquam.</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src={require('../../images/carousel_Img_1.jpg')} alt="" />
                                        <span className="recentTxt">Lorem ipsum dolor erat eget aliquam.</span>
                                    </a>
                                </li>
                                {/*<li>
                                    <a href="#">
                                        <img src={require('../../images/carousel_Img_2.jpg')} alt="" />
                                        <span className="recentTxt">Lorem ipsum dolor erat eget aliquam.</span>
                                    </a>
                                </li>*/}
                               {/* <li>
                                    <a href="#">
                                        <img src={require('../../images/carousel_Img_3.jpg')} alt="" />
                                        <span className="recentTxt">Lorem ipsum dolor erat eget aliquam.</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src={require('../../images/carousel_Img_1.jpg')} alt="" />
                                        <span className="recentTxt">Lorem ipsum dolor erat eget aliquam.</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src={require('../../images/carousel_Img_2.jpg')} alt="" />
                                        <span className="recentTxt">Lorem ipsum erat eget aliquam.</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src={require('../../images/carousel_Img_3.jpg')} alt="" />
                                        <span className="recentTxt">Lorem ipsum dolor erat eget aliquam.</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <img src={require('../../images/carousel_Img_1.jpg')} alt="" />
                                        <span className="recentTxt">Lorem ipsum dolor erat eget aliquam.</span>
                                    </a>
                                </li>*/}
                            </ul>
                        </div> 
                    </div> 


                </div>
                <Footer/>
            </div>



        );

    }


}


WelcomPage.propTypes = {
    actionMaster: PropTypes.object,

    /*
    actionMaster: PropTypes.object,
    actionOrg: PropTypes.object,
    organizationData: PropTypes.array,
    forDeleteData: PropTypes.object
*/
};

function mapStateToProps(state, ownProps) {
    return state.application;
}

function mapDispatchToProps(dispatch) {
    return {
        actionMaster: bindActionCreators(actionMaster, dispatch),
        //actionOrg: bindActionCreators(actionOrg, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(WelcomPage);
