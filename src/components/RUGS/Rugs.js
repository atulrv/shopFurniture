import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actionMaster from '../../actions/uiAction';
import Header from '../common/Header';


class Rugs extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    componentDidMount() {

    }

    render() {
        return (
            <div>
            <Header/>

            <div className="container" style={{marginTop:'150px'}}>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="heading">
                            <h1>Your Cart(2)</h1>
                        </div>
                        <div className="table-responsive">
                            <table className="table table-bordered cart-table">
                                <tr>
                                    <th className="text-center">Product Image</th>
                                    <th>Product</th>
                                    <th className="text-center">Price</th>
                                    <th className="text-center">Quantity</th>
                                    <th className="text-center">Total</th>
                                    <th className="text-center">Remove</th>
                                </tr>
                                <tr>
                                    <td className="cartTdStyle" style={{width: '20%'}}>
                                        <a href="#">
                                            <img src={require('../../images/carousel_Img_2.jpg')} alt=""/>

                                        </a>
                                    </td>
                                    <td className="valignMiddle" style={{width: '30%'}}>
                                        <h5>CUSHIONS</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    </td>
                                    <td className="cartTdStyle">$300</td>
                                    <td className="cartTdStyle"><input type="text" className="quanInput" value="1"/>
                                    </td>
                                    <td className="cartTdStyle red">$300</td>
                                    <td className="cartTdStyle"><a href="#"><i className="fa fa-times-circle fa-2x"
                                                                               aria-hidden="true"></i></a></td>
                                </tr>
                                <tr>
                                    <td className="cartTdStyle" style={{width: '20%'}}>
                                        <a href="#">
                                            <img src="images/carousel_Img_1.jpg" alt=""/>
                                        </a>
                                    </td>
                                    <td className="valignMiddle" style={{width: '30%'}}>
                                        <h5>RUGS</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    </td>
                                    <td className="cartTdStyle">$600</td>
                                    <td className="cartTdStyle"><input type="text" value="1" className="quanInput"/>
                                    </td>
                                    <td className="cartTdStyle red">$600</td>
                                    <td className="cartTdStyle"><a href="#"><i className="fa fa-times-circle fa-2x"
                                                                               aria-hidden="true"></i></a></td>
                                </tr>
                                <tr>
                                    <td className="cartTdStyle" style={{width: '20%'}}>
                                        <a href="#">
                                            <img src={require('../../images/carousel_Img_3.jpg')} alt=""/>
                                        </a>
                                    </td>
                                    <td className="valignMiddle" style={{width: '30%'}}>
                                        <h5>THROWS</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    </td>
                                    <td className="cartTdStyle">$1300</td>
                                    <td className="cartTdStyle"><input type="text" value="1" className="quanInput"/>
                                    </td>
                                    <td className="cartTdStyle red">$1300</td>
                                    <td className="cartTdStyle"><a href="#"><i className="fa fa-times-circle fa-2x"
                                                                               aria-hidden="true"></i></a></td>
                                </tr>

                            </table>
                        </div>

                        <div className="table-responsive">
                            <table className="table table-bordered cart-table">
                                <tr>
                                    <td className="text-center valignMiddle" colspan="4">Sub Total</td>
                                    <td className="cartTdStyle red">$2200</td>
                                    <td className="cartTdStyle"></td>
                                </tr>
                                <tr>
                                    <td className="text-center valignMiddle" colspan="4">Shipping</td>
                                    <td className="cartTdStyle red">$0.00</td>
                                    <td className="cartTdStyle"></td>
                                </tr>
                                <tr>
                                    <td className="text-center valignMiddle" colspan="4">Tax</td>
                                    <td className="cartTdStyle red">$100</td>
                                    <td className="cartTdStyle"></td>
                                </tr>
                                <tr>
                                    <td className="text-center valignMiddle" colspan="4">Grand Total</td>
                                    <td className="cartTdStyle red">$2300</td>
                                    <td className="cartTdStyle"><a href="checkout.html"
                                                                   className="splReverseBtn">Checkout</a></td>
                                </tr>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

            </div>
        )
            ;
    }

}

Rugs.propTypes = {
    actionMaster: PropTypes.object,
};

function mapStateToProps(state, ownProps) {
    return state.application;
}

function mapDispatchToProps(dispatch) {
    return {
        actionMaster: bindActionCreators(actionMaster, dispatch),
        //actionOrg: bindActionCreators(actionOrg, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Rugs);
