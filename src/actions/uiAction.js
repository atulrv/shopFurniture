export function showConfirmation(data) {
    return {type: "SHOW_CONFIRMATION_FOR_UPLOAD", data};
}
export function ShowLogin(data) {
    return {type: "SHOW_LOGIN", data};
}
export function HideLogin(data) {
    return {type: "HIDE_LOGIN", data};
}
