import React from 'react';
import {Route, IndexRoute} from 'react-router';
import App from './components/app';
import WelcomPage from './components/Home/welcome';
import Rugs from './components/RUGS/Rugs';

export default (
    <Route path="/" component={App}>
        <IndexRoute component={WelcomPage}/>
        <path path="Rugs" component={Rugs} />
    </Route>
);